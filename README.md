# Ejercicios Conexión PHP

## Ejercicio 1

Entrega como resultado TRUE o FALSE para cierto valor entregado.
El parámetro lo entregamos a través del llamado a la funcion en la URL como en el ejemplo.

```
http://3.144.197.204/conexion/ejercicio1.php?parametro=8
```

RESPUESTA A LAS PREGUNTAS

A- Qué problemas de eficiencia tiene su algoritmo con número grandes? 
El problema principal es que se almacena una variable int exponencial que podria agotar el espacio de memoria.

B- Utilizando funciones matemáticas. 
Por un lado, utilizar la función logarítmica para validar que el parámetro ingresado sea potencia de 2, con el obtenemos el número exacto por el que hay que elevar 2 para obtener dicho valor, nos entrega valor entero si corresponde y decimal si no es así.
Lo anterior lo validamos con el modulo del número obtenido, para cero es true y otro false.

if( fmod( log($x,2) ,1 ) == 0) return true

## Ejercicio 2

El desarrollo en base a un arreglo de números desordenados este crea una nuevo arreglo con los valores ordenados.
```
http://3.144.197.204/conexion/ejercicio2.php
```
