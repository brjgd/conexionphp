<!DOCTYPE html>
<html>
    <head>
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php
        $resultado=false;
        $parametro=$_GET['parametro'];
        function comprobar($parametro){
            if($parametro < 1)
                return false;
            if($parametro <= 2){
                return true;
            }
            $i = 2;
            while(true){
                $i *= 2;
                if ($i == $parametro){
                    return true;
                }
                if ($i > $parametro){
                    return false;
                }
        }
    }
        ?>
    <p>el resultado es <?=json_encode(comprobar($parametro))?> </p>
    </body>
</html>
