<!DOCTYPE html>
<html>
    <head>
        <title>Ejercicio 2</title>
    </head>
    <body>
    
    <?php

        class Arreglo
        {


            function ordenarArreglo($arreglo){
                $longitud = count($arreglo);
                for ($i = 0; $i<$longitud; $i++) {
                    for ($j = 0; $j < $longitud - 1; $j++) {
                        if ($arreglo[$j] > $arreglo[$j + 1]) {
                            $temporal = $arreglo[$j];
                            $arreglo[$j] = $arreglo[$j + 1];
                            $arreglo[$j + 1] = $temporal;
                        }
                    }          
                }
                return $arreglo;
            }
            function imprimirArreglo($arreglo){
                $longitud = count($arreglo);
                for($i=0; $i<$longitud; $i++){
                    echo("[".$arreglo[$i]."] ");
                }
                echo("<br>");
            }
        }

        $arregloBase = [7,2,4,8,3,9,1,5,10,6];
        echo("Arreglo Base: ");

        $lista = new Arreglo;
        $lista->imprimirArreglo($arregloBase);   

        $nuevaArreglo = $lista->ordenarArreglo($arregloBase);
        echo("Nuevo Arreglo: ");       
        $lista->imprimirArreglo($nuevaArreglo);
    ?>
    </body>
</html>

